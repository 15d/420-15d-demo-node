"use strict";

const http = require('http');

const innerHtml = `<!DOCTYPE html>
<html lang="fr">
<head>
<title>Enter Message</title>
</head>
<body>
<form action="/message" method="POST"><input type="text" name="message"><button type="submit">Send</button>
</form>
</body>
</html>`;

const server = http.createServer((req, res) => {
  // Fonction de callback exécutée quand une requête est reçu par le serveur
  const { method, url } = req;

  if (url === '/') {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.statusCode = 200;
    res.write(innerHtml);
    // Termine la réponse et l'envoie au client
    return res.end();
  }
  if (url === '/message' && method === 'POST') {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.statusCode = 200;
    res.write('<html>');
    res.write('<head><title>Résultat</title><head>');
    res.write('<body><h2>Merci !</h2></body>');
    res.write('</html>');
    res.end();
  } else {
    res.statusCode = 404;
    res.end("page 404");
  }
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur le port %s ', server.address().port);
});