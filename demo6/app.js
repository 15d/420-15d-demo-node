"use strict";

const http = require("http");

const html = `<html lang="fr">
<head>
<title>Blogue</title>
</head>
<body>
    <h1>Bienvenue sur mon blogue</h1>
</body>
</html>`;

const server = http.createServer((req, res) =>{

	res.setHeader("Content-type", "text-html; charset=utf-8 ");
	res.statusCode = 200;
	res.write(html);
	res.end();

});

server.listen(3000, ()=>{
	console.log("Node.js est à l'écouter sur http://localhost:" + server.address().port);
});
