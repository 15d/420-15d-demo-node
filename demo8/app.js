"use strict";

const http = require('http');
const url = require('url');

const server = http.createServer((req, res) => {

  const q = url.parse(req.url, true);

  console.log('host', req.headers.host); // localhost:3000
  console.log('href : ', q.href); // /index.html?post=15&cat=chien
  console.log('pathname : ', q.pathname); // /index.html
  console.log('search : ', q.search); // ?post=15&cat=chien 

  const qdata = q.query; // { post: '15', cat: 'chien' }
  console.log('post : ', qdata.post); // 15 
  console.log('cat : ', qdata.cat); // chien 
  res.statusCode = 200;
  res.end();
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur le port %s ', server.address().port);
});

// test avec une url, exemple:
// 'http://localhost:3000/index.html?post=15&cat=chien' 