"use strict";

const http = require('http');

const server = http.createServer((req, res) => {
  const url = req.url;
  const method = req.method;
  if (url === '/') {
    res.write('<html>');
    res.write('<head><title>Message</title><head>');
    res.write('<body><form action="/message" method="POST"><input type="text" name="message"><button type="submit">Soumettre</button></form></body>');
    res.write('</html>');
    return res.end();
  }
  if (url === '/message' && method === 'POST') {
    const body = [];
    // Événement qui se déclenche lorsqu'une partie de la requête est reçue
    // data est un événement spécial qui se déclenche lorsqu'une partie de la requête est reçue
    req.on('data', (chunk) => {
      console.log(chunk);
      // console.log(chunk.toString());
      body.push(chunk);
    });
    // Événement qui se déclenche lorsque la requête est terminée
    req.on('end', () => {
      const parsedBody = Buffer.concat(body).toString();
      const message = parsedBody.split('=')[1];
      res.setHeader('Content-Type', 'text/html; charset=utf-8');
      return res.end(message);
    });
  }
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur le port %s ', server.address().port);
});
