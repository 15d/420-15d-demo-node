"use strict";

const http = require('http');

// Création du serveur et gestionnaire d'événement passé en paramètre
const server = http.createServer((req, res) => {
  // Fonction de callback exécutée quand une requête est reçu par le serveur
  // console.log('req', req);

  // Affichage 
  const { method, url, headers } = req;
  console.log('url ====> ', url);
  console.log('method ====> ', method);
  console.log('headers ====> ', headers);

  // La méthode exit() permet de terminer le processus Node.js
  // process.exit();
});

server.listen(3000, () => {
  console.log('Node.js est à l\'écoute sur le port %s ', server.address().port);
});