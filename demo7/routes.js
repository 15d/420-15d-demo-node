"use strict";

const requestHandler = (req, res) => {

  const innerHtml = `<!DOCTYPE html>
<html lang="fr">
<head>
<title>Enter Message</title>
</head>
<body>
<form action="/message" method="POST"><input type="text" name="message"><button type="submit">Send</button>
</form>
</body>
</html>`;

  const url = req.url;
  const method = req.method;

  if (url === '/') {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.statusCode = 200;
    res.write(innerHtml);
    return res.end();
  }
  if (url === '/message' && method === 'POST') {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');
    res.statusCode = 200;
    res.write('<html>');
    res.write('<head><title>Résultat</title><head>');
    res.write('<body><h2>Merci !</h2></body>');
    res.write('</html>');
    res.end();
  }

};

module.exports = requestHandler;

